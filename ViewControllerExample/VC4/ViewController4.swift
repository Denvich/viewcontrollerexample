//
//  ViewController4.swift
//  ViewControllerExample
//
//  Created by Denis Lyakhovich on 21.08.2021.
//

import UIKit

class ViewController4: UIViewController {

    @IBOutlet weak var buttonClose: UIButton!
    //обработчик обратного перехода по нажатию на кнопкуbattonClose
    @IBAction func buttonCloseVC4(_ sender: Any) {
        //метод dismiss рабтает при переходе без использования сегвей
        dismiss(animated: true, completion: nil)
    }
    //обработчик обратного перехода по нажатию на кнопку - стрелка
    @IBAction func arrowCloseVC4(_ sender: Any) {
        //метод dismiss рабтает при переходе без использования сегвей
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //округление углов у кнопки close
        buttonClose.layer.cornerRadius = 5
    }
    
    //переход в данный VC4 осуществляется програмно из VC1, при помощи нажатия на кнопку VC4 и вызова метода prepare в VC1, без использования сегвей
}




