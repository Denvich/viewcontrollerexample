//
//  ViewController5.swift
//  ViewControllerExample
//
//  Created by Denis Lyakhovich on 22.08.2021.
//

import UIKit

class ViewController5: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    //категории списка рецептов в виде свойства
    var caterories = RecipeCaterory.allRecipes
    
    //динамическое изменение контента
    //свойство для перезагрузки ячейки без применения метода reloadData - если контент динамический
    var reuseIdentifire = "recipe cell"
    //динамическое изменение контента
    
    //переопределяем метод viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        //регистрируем ячейку xib
        tableView.register(UINib(nibName: "RecipeTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifire)
    }
    
    //метод возвращает количество секций в таблице
    func numberOfSections(in tableView: UITableView) -> Int {
        return caterories.count
    }
    
    //numberOfRowsInSection обязательный метод который должен реализовываться протоколом UITableViewDataSource, возвращает количетво строк из общего массива данных allRecipes
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return caterories[section].recipe.count
    }
    //cellForRowAt indexPath обязательный метод который должен реализовывать протоколом UITableViewDataSource, определяет ячейку
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //подключаем ячейку
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifire, for: indexPath) as! RecipeTableViewCell
        
        //получаем рецепт
        let recipe = caterories[indexPath.section].recipe[indexPath.row]
        //натраиваем поля в ячейке
        cell.recipeTitleLabel.text = recipe.title
        cell.recipeIngredientsLabel.text = recipe.ingredients
        cell.iconImageView.image = recipe.photo
        
        return cell
        
        /*_____________________________Не используем поскольку подключаем ячейку xib___________________
        //__________динамическое изменение контента___________________
        //извлекаем не нужную ячейку,без применения метода reloadData - если контент динамический
        var cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: reuseIdentifire)
        //событе если ячейка не найдена
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: reuseIdentifire)
            cell.textLabel?.numberOfLines = 0
            cell.detailTextLabel?.numberOfLines = 0
            print("New cell")
        } else {
            print("Reused")
        }
        //__________динамическое изменение контента___________________
        
        
        //создаем констатнту ячейку subtitle переносим в if
        //let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        //получаем контент в ячейку из массива allRecipes
        
        
        let recipe = caterories[indexPath.section].recipe[indexPath.row]
        //настраиваем ячейку данными
        cell.textLabel?.text = recipe.title
        //параметр 0 означает что количество строк  в заголовке может быть не ограниченным (для того чтобы списки вмещались полностью) переносим if
        //cell.textLabel?.numberOfLines = 0
        cell.detailTextLabel?.text = recipe.ingredients
        //параметр 0 означает что количество строк  в детализазции может быть не ограниченным (для того чтобы списки вмещались полностью) переносим if
        //cell.detailTextLabel?.numberOfLines = 0
        //возвращаем наполненную ячейку в таблицу
        return cell
         _____________________________Не используем поскольку подключаем ячейку xib___________________ */
        
        
    }
    
    //подписываем заголовки категорий(секций) таклицы
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return caterories[section].title
    }
        
}
 


