//
//  Recipe.swift
//  ViewControllerExample
//
//  Created by Denis Lyakhovich on 22.08.2021.
//

import UIKit

//MARK: - структура данных для tableView

//структура определяюшая категории tableView
struct RecipeCaterory {
    var title: String
    var recipe: [Recipe]
}

//структура определяющая состав рецептов tableView
struct Recipe {
    var photo: UIImage?
    var title: String
    var ingredients: String
}

//расширение струтуры данных, создание общего массива данных allRecipes и его инициализация с категориям и описанием рецепов
extension RecipeCaterory {
    static var allRecipes: [RecipeCaterory] {
        return [
            RecipeCaterory(title: "Супы", recipe: [
                            Recipe(photo: UIImage(named: "1"), title: "Суп-Харчо", ingredients: "Говядина, рис, лук, морковь, капуста, картофель, свекла"),
                            Recipe(photo: nil, title: "Суп-по-Русски", ingredients: "Свинина, лук, морковь, капуста, картофель"),
                            Recipe(photo: nil, title: "Суп-Куриный", ingredients: "Курица, лук, морковь, картоффель")
            ]),
            RecipeCaterory(title: "Борщи", recipe: [
                            Recipe(photo: UIImage(named: "2"), title: "Борщ-Украинский", ingredients: "Говядина, свинина, лук, морковь, капуста, картофель, свекла, укроп"),
                            Recipe(photo: nil, title: "Борщь-Уральский", ingredients: "Свинина, лук, морковь, капуста, картофель, петрушка"),
                            Recipe(photo: nil, title: "Борщь-Русский", ingredients: "Свинина, лук, морковь, капуста, картоффель, перец болгарский, укроп, петрушка")
            ]),
            RecipeCaterory(title: "Солянки", recipe: [
                            Recipe(photo: UIImage(named: "3"), title: "Солянка-Бодрая", ingredients: "Говядина, свинина, колбаса сырокопчена, лук, морковь, огурец, оливки, лимон картофель, свекла, укроп"),
                            Recipe(photo: nil, title: "Солянка-Финская", ingredients: "Свинина, говядина, форель, лук, морковь, капуста, картофель, оливки, лимон"),
                            Recipe(photo: nil, title: "Солянка-Американская", ingredients: "Рулька свиная, колбаса вареная, колбаса сырокопчена, лук, морковь, капуста, картоффель, перец болгарский, укроп, петрушка, лимон, оливки")
            ]),
            RecipeCaterory(title: "Уха", recipe: [
                            Recipe(photo: UIImage(named: "4"), title: "Уха-Из-Петуха", ingredients: "Петух,семга, лук, морковь, оливки, лимон картофель, укроп"),
                            Recipe(photo: nil, title: "Уха-Финская", ingredients: "Семга, форель, кета, лук, морковь, картофель, оливки, лимон"),
                            Recipe(photo: nil, title: "Уха-Русская", ingredients: "Карп, сазан, щука, лук, морковь, картоффель, перец болгарский, укроп, петрушка, лимон, оливки")
            ])
        ]
    }
}
