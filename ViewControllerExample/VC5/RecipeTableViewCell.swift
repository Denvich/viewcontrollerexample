//
//  RecipeTableViewCell.swift
//  ViewControllerExample
//
//  Created by Denis Lyakhovich on 22.08.2021.
//

import UIKit

class RecipeTableViewCell: UITableViewCell {
    
   
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var recipeTitleLabel: UILabel!
    @IBOutlet weak var recipeIngredientsLabel: UILabel!
    
    //метод похож на метод viewDidLoad - здесь задаются настройки
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
