//
//  ViewController2.swift
//  ViewControllerExample
//
//  Created by Denis Lyakhovich on 20.08.2021.
//

import UIKit

class ViewController2: UIViewController {
    
    @IBOutlet weak var buttonNext: UIButton!
    
    //переход по сегвей во viewcontroller3
    @IBAction func buttonTappted(_ sender: UIButton) {
        performSegue(withIdentifier: "goToVc3", sender: nil)
    }
    
    //передача данный во viewcontroller3 по сегвей
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? ViewController3, segue.identifier == "goToVc3" {
            viewController.text = "?"
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        buttonNext.layer.cornerRadius = 5
    }
}
