//
//  ViewController3.swift
//  ViewControllerExample
//
//  Created by Denis Lyakhovich on 20.08.2021.
//

import UIKit

class ViewController3: UIViewController {

    @IBOutlet weak var buttonStop: UIButton!
    //аутлет для передачи данных по сегвей
    @IBOutlet weak var textLabel: UILabel!
    //свойство для передачи данных по сегвей из контроллера2
    var text: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonStop.layer.cornerRadius = 5
        //отображение переданных данных по сегвей из VC2 d VC3, через свойство text
        textLabel.text = text
    }
}
