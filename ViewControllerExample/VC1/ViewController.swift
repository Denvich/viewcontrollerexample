//
//  ViewController.swift
//  ViewControllerExample
//
//  Created by Denis Lyakhovich on 20.08.2021.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonVC4: UIButton!
    @IBOutlet weak var buttonTable: UIButton!
    @IBOutlet weak var buttonCollection: UIButton!
    
    //обработчик обратного перехода (метод возвращает на стартовый вью по сегвей. )
    @IBAction func unwindSegue(_ sender: UIStoryboardSegue) {
        print("Exit")
    }
    
    @IBAction func goToViewControllr4(_ sender: Any){
        //создаем константу которая инициализирует view из xib файла
        let viewController4 = ViewController4(nibName: "ViewController4", bundle: nil)
        //вызываем метод present и передаем в него констату с xib файлом
        present(viewController4, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //округление углов кнопок черезсвойство layer
        buttonNext.layer.cornerRadius = 5
        buttonVC4.layer.cornerRadius = 5
        buttonTable.layer.cornerRadius = 5
        buttonCollection.layer.cornerRadius = 5
    }
}

